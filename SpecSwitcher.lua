if not (IsAddOnLoaded("Tukui") or IsAddOnLoaded("AsphyxiaUI") or IsAddOnLoaded("DuffedUI") or IsAddOnLoaded("ElvUI")) then return end
local SpecTalent = CreateFrame("Button", "SpecTalent", UIParent)
SpecTalent:SetTemplate()
SpecTalent:EnableMouse(true)
SpecTalent:SetMovable()
SpecTalent:Size(27)
SpecTalent:CreateBackdrop("Transparent")
SpecTalent.backdrop:SetOutside(SpecTalent, 4, 4)
SpecTalent.backdrop:CreateShadow()
SpecTalent:Point("CENTER", UIParent, "CENTER", 0, 0)
SpecTalent:RegisterForClicks("LeftButtonDown", "RightButtonDown")
SpecTalent:RegisterForDrag("RightButton")
SpecTalent:SetScript("OnDragStart", function(self) self:StartMoving() end)
SpecTalent:SetScript("OnDragStop", function(self) self:StopMovingOrSizing() end)
SpecTalent:RegisterEvent("PLAYER_ENTERING_WORLD")
SpecTalent:RegisterEvent("ACTIVE_TALENT_GROUP_CHANGED")
SpecTalent:RegisterEvent("PLAYER_TALENT_UPDATE")
SpecTalent:RegisterEvent("PLAYER_LEVEL_UP")

local SpecTalentIcon = SpecTalent:CreateTexture(nil, "ARTWORK")
SpecTalentIcon:SetInside(SpecTalent)
SpecTalentIcon:SetTexCoord(0.08, 0.92, 0.08, 0.92)

SpecTalent:SetScript("OnEvent", function(self, event, level)
	EnableSpecSwitcherSpamFilter()
	if (event == "PLAYER_LEVEL_UP") then
		if ((level or UnitLevel("player")) < 10) then
			SpecTalent:Hide()
			return
		else
			SpecTalent:Show()
		end
	else
		if (UnitLevel("player") < 10) then
			SpecTalent:Hide()
			return
		else
			SpecTalent:Show()
		end
	end
	local primary = GetSpecialization()
	if primary ~= nil then
		local texture = select(4, GetSpecializationInfo(primary))
		local currentSpecName = primary and select(2, GetSpecializationInfo(primary)) or "None"
		local inInstance, instanceType = IsInInstance()
		SpecTalentIcon:SetTexture(texture)
		if (event == "ACTIVE_TALENT_GROUP_CHANGED") then
			UseEquipmentSet(currentSpecName)
		end
		if (instanceType == "party" or instanceType == "raid") then
			UseEquipmentSet(currentSpecName)
		end
		if (instanceType == "pvp" or instanceType == "arena") then
			UseEquipmentSet("PvP")
		end
	else
		local texture = select(3, GetSpellInfo(63624))
		SpecTalentIcon:SetTexture(texture)
	end
end)
SpecTalent:SetScript("OnClick", function(self, btn)
	if btn == 'LeftButton' then
		if not InCombatLockdown() then
			local spec = GetActiveSpecGroup()
			if spec == 1 then
				SetActiveSpecGroup(2)
			else
				SetActiveSpecGroup(1)
			end
		else
			print("Please Exit Combat before trying to switch your Specialization.")
		end
	else
	end
end)
PetBattleFrame:HookScript("OnShow", function() SpecTalent:Hide() end)
PetBattleFrame:HookScript("OnHide", function() SpecTalent:Show() end)

local function SpecSwitcherSpamFilter(self, event, msg, ...)
	if strfind(msg, string.gsub(ERR_LEARN_ABILITY_S:gsub('%.', '%.'), '%%s', '(.*)')) then
		return true
	elseif strfind(msg, string.gsub(ERR_LEARN_SPELL_S:gsub('%.', '%.'), '%%s', '(.*)')) then
		return true
	elseif strfind(msg, string.gsub(ERR_SPELL_UNLEARNED_S:gsub('%.', '%.'), '%%s', '(.*)')) then
		return true
	elseif strfind(msg, string.gsub(ERR_LEARN_PASSIVE_S:gsub('%.', '%.'), '%%s', '(.*)')) then
		return true
	end
    
	return false, msg, ...
end

function EnableSpecSwitcherSpamFilter()
	ChatFrame_AddMessageEventFilter("CHAT_MSG_SYSTEM", SpecSwitcherSpamFilter)
end

function DisableSpecSwitcherSpamFilter()
	ChatFrame_RemoveMessageEventFilter("CHAT_MSG_SYSTEM", SpecSwitcherSpamFilter)
end