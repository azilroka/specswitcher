## Interface: 50200
## Title: |cffC495DDTukui|r & |cff1784d1ElvUI|r Spec Switcher
## Version: 1.05
## Notes: Provides an icon of your Specialization and the ability to switch between your Specializations.
## OptionalDeps: Tukui, ElvUI, Tukui_Skins, DuffedUI, AsphyxiaUI
## Author: Azilroka
## X-Tukui-ProjectID: 126
## X-Tukui-ProjectFolders: SpecSwitcher

SpecSwitcher.lua